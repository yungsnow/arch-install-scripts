#!/bin/bash

# Base installation
pacman -S --noconfirm reflector
reflector -c Germany -a 15 -p https --sort rate --save /etc/pacman.d/mirrorlist
pacman -Syy
pacman -S --noconfirm base-devel linux-lts sysfsutils usbutils e2fsprogs inetutils netctl nano vim less which man-db man-pages amd-ucode

# Installing grub
pacman -S --noconfirm grub os-prober
grub-install --target=i386-pc --recheck /dev/sda
grub-mkconfig -o /boot/grub/grub.cfg

# Setup hostname
echo "arch" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 arch.localdomain arch" >> /etc/hosts

# Setup locale
sed -i '177s/.//' /etc/locale.gen
sed -i '178s/.//' /etc/locale.gen
sed -i '133s/.//' /etc/locale.gen
sed -i '134s/.//' /etc/locale.gen
sed -i '135s/.//' /etc/locale.gen
locale-gen
echo "LANG=de_DE.UTF-8" > /etc/locale.conf
export "LANG=de_DE.UTF-8"
echo "KEYMAP=de-latin1" > /etc/vconsole.conf

# Setup time & date
ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
hwclock --systohc --utc

# Configure pacman
sed -i '93s/.//' /etc/pacman.conf
sed -i '94s/.//' /etc/pacman.conf
pacman -Syu

# Setup users
echo root:password | chpasswd
useradd -m -G audio,video,input,wheel,sys,log,rfkill,lp,adm -s /bin/bash marcel
echo marcel:password | chpasswd
echo "marcel ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers.d/marcel

# Useful packages
pacman -S --noconfirm linux-headers linux-lts-headers dkms
pacman -S --noconfirm jshon expac wget acpid avahi net-tools xdg-user-dirs
systemctl enable acpid systemd-timesyncd

# IF SYSTEM IS RUNNUNG ON A SSD
#systemctl enable fstrim.timer

# Networking
pacman -S --noconfirm networkmanager networkmanager-openvpn networkmanager-pptp networkmanager-vpnc
systemctl enable NetworkManager

printf "\e[1;32mDone! Type exit, umount -a and reboot. Don't forget to change the password.\e[0m"