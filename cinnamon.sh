#!/bin/bash

# Install Display Server
sudo pacman -S --noconfirm xorg-server xorg-xinit xorg-xrandr xorg-xfontsel xorg-xlsfonts xorg-xkill xorg-xinput xorg-xwininfo

# Install Desktop Environment
sudo pacman -S --noconfirm cinnamon cinnamon-translations nemo-fileroller nemo-image-converter nemo-preview xed xreader gnome-terminal metacity gnome-shell gnome-screenshot firefox arc-gtk-theme papirus-icon-theme

# Install Display/Desktop Manager
sudo pacman -S --noconfirm lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings
sudo systemctl enable lightdm

# Install Input Driver
sudo pacman -S --noconfirm xf86-input-synaptics xf86-input-libinput xf86-input-evdev
# When installing inside a virtual machine:
sudo pacman -S --noconfirm xf86-input-vmmouse

# Graphics Driver
sudo pacman -S --noconfirm mesa lib32-mesa
sudo pacman -S --noconfirm vulkan-icd-loader lib32-vulkan-icd-loader

# Open Source drivers
#sudo pacman -S --noconfirm xf86-video-amdgpu
#sudo pacman -S --noconfirm xf86-video-nouveau
#sudo pacman -S --noconfirm xf86-video-intel
#sudo pacman -S --noconfirm xf86-video-ati
#sudo pacman -S --noconfirm xf86-video-vmware
#sudo pacman -S --noconfirm xf86-video-fbdev

# Nvidia proprietary driver
#sudo pacman -S --noconfirm nvidia nvidia-lts nvidia-utils lib32-nvidia-utils libvdpau lib32-libvdpau

# AMD Utils
#sudo pacman -S --noconfirm libva-mesa-driver lib32-libva-mesa-driver mesa-vdpau lib32-mesa-vdpau libva-vdpau-driver lib32-libva-vdpau-driver amdvlk lib32-amdvlk

# Intel Utils
#pacman -S --noconfirm vulkan-intel

mkinitcpio -P

# Sound
sudo pacman -S --noconfirm alsa-utils pulseaudio-alsa pulseaudio-equalizer pavucontrol

# Media Codecs
sudo pacman -S --noconfirm gst-libav gst-plugins-base gst-plugins-good gst-plugins-bad gst-plugins-ugly gstreamer-vaapi gst-transcoder x265 x264 lame

# Printer support
sudo pacman -S --noconfirm system-config-printer foomatic-db foomatic-db-engine gutenprint gsfonts cups cups-pdf cups-filters sane simple-scan
sudo systemctl enable cups.service saned.socket

# Bluetooth support
sudo pacman -S --noconfirm bluez bluez-utils pulseaudio-bluetooth
sudo systemctl enable bluetooth

/bin/echo -e "\e[1;32mREBOOTING IN 5..4..3..2..1..\e[0m" 
sleep 5 
sudo reboot