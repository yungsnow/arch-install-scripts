# Set X11 keymap
sudo localectl set-x11-keymap yourkeymap

# Some archive and file system utils
pacman -S --noconfirm p7zip unrar unarchiver unzip unace xz rsync
pacman -S --noconfirm nfs-utils cifs-utils ntfs-3g exfat-utils

# AUR
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -rsi
cd .. && rm -rf yay

# General Fonts
yay -S --noconfirm adobe-source-sans-pro-fonts ttf-dejavu ttf-opensans noto-fonts freetype2 terminus-font ttf-bitstream-vera ttf-dejavu ttf-droid ttf-fira-mono ttf-fira-sans ttf-freefont ttf-inconsolata ttf-liberation ttf-linux-libertine

# Decrease swappiness
#vm.swappiness=10

